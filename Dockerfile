FROM golang:1.16.4-alpine3.13
LABEL author="Juha Ristolainen <juha.ristolainen@iki.fi>"

ARG USER=dev
ENV HOME /home/$USER

ENV GOLANGCILINT_VERSION 1.40.1
ENV GORELEASER_VERSION 0.164.0
ENV CGO_ENABLED 0

# Install a few needed tools
RUN apk update && apk add sudo git curl openssh make gcc libc-dev

# Add non-root user
RUN adduser -D $USER \
    && echo "$USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USER \
    && chmod 0440 /etc/sudoers.d/$USER

# Install goreleaser
RUN wget https://github.com/goreleaser/goreleaser/releases/download/v$GORELEASER_VERSION/goreleaser_amd64.apk \
    && apk --no-cache --allow-untrusted add goreleaser_amd64.apk

# Install golangci-lint
RUN wget -O- -nv https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s v$GOLANGCILINT_VERSION

USER $USER
WORKDIR $HOME


